package com.example.textviewer

import android.widget.TextView

object TempObject {
    fun setTextForTextView(text: String, view: TextView) {
        view.text = text
    }
}